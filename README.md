# OVERVIEW #

Docker installer script for RHEL derivated (ver. 7) distros.

It, 

- Installs docker from the official yum repo
- Disables selinux
- Replaces firewalld with iptables
- Asks you if this will be a swarm node then inserts necessary firewall rules for swarm communication.

## Usage ##

This example installs docker and configures it by using /dev/sdb to create a LVM thin-pool as devicemapper storage.
```
bash install.sh --disk /dev/sdb
```

## Important Notes ##

This script is designed to install and configure docker on a *fresh* RHEL/CentOS systems.
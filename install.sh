#!/usr/bin/env bash

# Install necessary packages first.
if [ -z `which jq` ]; then
    curl http://stedolan.github.io/jq/download/linux64/jq -o /usr/local/bin/jq
    chmod +x /usr/local/bin/jq
fi

if [ -z `which lvm` ]; then
    yum install -y lvm2
fi

# Define Variables
DISK_PATTERN="^\/dev\/([a-z]|[A-Z]){3,4}$|^\/dev\/([a-z]|[A-Z]){3,4}([1-9]{1})$"
DM_DISK="$2"
CHECK_DOCKER=$(rpm -qa |egrep -i docker)
CHECK_DISK=$(fdisk -l |egrep "$2")
CHECK_PV=$(pvscan |egrep "PV $2")
I="iptables"


# Check root privilege
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

# Check 2 arguments are given
if [ $# -ne 2 ]; then
    echo "Usage : $0 --disk /dev/sdX"
    exit 1
fi

if [[ $2 =~ $DISK_PATTERN ]]
then
    if [[ $2 =~ (^/dev/sda$|^/dev/sda1|^/dev/sda2|^/dev/xvda$|^/dev/xvda1|^/dev/xvda2) ]]; then
        echo "Boot, root and swap partitions are not usable."
        echo "Here are usable disks"
        fdisk -l |egrep "/dev/sd|dev/xvd" |egrep -v "sda:|sda1|sda2|xvda:|xvda1|xvda2"
        echo "Please choise a correct one!"
        exit 1
    fi
    if [[ -z `fdisk -l |egrep "$2"` ]]; then
        echo "There is no disk named $2. Please check your disks"
        exit 1
    fi

    case "$1" in

    --disk) echo "Using $2 to create devicemapper storage for docker"

        if [[ -n "$CHECK_DOCKER" ]]; then
cat << EOF
"Docker is already installed.
This script designed to install docker from
scratch on a fresh RHEL derivated distros.
Please remove Docker manually..."
EOF
            exit 1
        fi

        yum -y install git curl bridge-utils
        echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf
        # fix for swarm network broken state as mentioned in
        # https://github.com/docker/docker/issues/31208
        echo "net.ipv4.tcp_keepalive_time=600" >> /etc/sysctl.conf
        sysctl -p
        sed -i -e 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config && setenforce 0
        systemctl mask firewalld
        systemctl stop firewalld
        yum -y install iptables-services
        systemctl enable iptables
        systemctl enable ip6tables

        sed -i -e 's/IPTABLES_SAVE_ON_STOP="no"/IPTABLES_SAVE_ON_STOP="yes"/g' /etc/sysconfig/iptables-config
        sed -i -e 's/IPTABLES_SAVE_ON_RESTART="no"/IPTABLES_SAVE_ON_RESTART="yes"/g' /etc/sysconfig/iptables-config
        systemctl start iptables
        systemctl start ip6tables

cat > /etc/yum.repos.d/docker.repo <<-'EOF'
[dockerrepo]
name=Docker Repository
baseurl=https://yum.dockerproject.org/repo/main/centos/7/
enabled=1
gpgcheck=1
gpgkey=https://yum.dockerproject.org/gpg
EOF

        yum -y install docker-engine
        systemctl enable docker
        curl -L "https://github.com/docker/compose/releases/download/1.19.0/docker-compose-$(uname -s)-$(uname -m)" > /usr/local/bin/docker-compose
        chmod +x /usr/local/bin/docker-compose

        if [[ -n "$CHECK_PV" ]]; then
cat << EOF
"It seems "$DM_DISK" already is in use as a
Physical Volume Please set an unused disk
to "DM_DISK" variable above. "
EOF
        exit 1
        fi

        echo "We're going to create LVM thin-pool which will be used as Docker's devicemapper storage"
        pvcreate $DM_DISK
        vgcreate docker $DM_DISK
        lvcreate --wipesignatures y -n thinpool docker -l 95%VG
        lvcreate --wipesignatures y -n thinpoolmeta docker -l 1%VG
        lvconvert -y --zero n -c 512K --thinpool docker/thinpool --poolmetadata docker/thinpoolmeta


cat > /etc/lvm/profile/docker-thinpool.profile <<-'EOF'
activation {
    thin_pool_autoextend_threshold=80
    thin_pool_autoextend_percent=20
}
EOF

        lvchange --metadataprofile docker-thinpool docker/thinpool

        mkdir /etc/docker && chmod 700 /etc/docker
cat > /etc/docker/daemon.json <<-'EOF'

{
  "storage-driver": "devicemapper",
   "storage-opts": [
     "dm.thinpooldev=/dev/mapper/docker-thinpool",
     "dm.basesize=50G",
     "dm.use_deferred_removal=true",
     "dm.use_deferred_deletion=true"
   ]
}
EOF

        chmod 600 /etc/docker/daemon.json
        systemctl start docker

        # Configure iptables
        echo "Waiting for 15 seconds to docker daemon to create its bridges"
        sleep 15

        # Save rules on stop and restart
        sed -i -e 's/IPTABLES_SAVE_ON_STOP="no"/IPTABLES_SAVE_ON_STOP="yes"/g' /etc/sysconfig/iptables-config
        sed -i -e 's/IPTABLES_SAVE_ON_RESTART="no"/IPTABLES_SAVE_ON_RESTART="yes"/g' /etc/sysconfig/iptables-config

        function add_base_iptables_rules {

            echo "Adding basic iptables pass rules..." && sleep 3
            SSH_PORT=$(ss -antp |egrep -i ssh |egrep LISTEN |awk '{print $4}' |tr -d '*|:' |head -1)
            $I -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
            $I -A INPUT -p icmp -j ACCEPT
            $I -A INPUT -i lo -j ACCEPT
            $I -A INPUT -p tcp -m state --state NEW -m tcp --dport $SSH_PORT -j ACCEPT
            $I -A INPUT -j REJECT --reject-with icmp-host-prohibited
        }

        function add_swarm_iptables_rules {

            echo "Swarm mode rules are creating..." && sleep 3

            # Set necessary swarm iptables rules for instance's default network.
            # Is this GCE instance ?
            cat /etc/group |egrep google
            if [ $? == "0" ]; then

            NETWORK=$(ip a |egrep inet |egrep "scope global " |egrep -v \
            "secondary |scope global docker|scope global br-" |awk '{print $2}')
            NETWORK=$(echo $NETWORK |sed "s#\/32#\/20#g")
            SOURCE_NETWORK="$NETWORK"
            else

            # Catch primary IP
            IP=$(ip a |egrep inet |egrep "scope global " |egrep -v \
            "secondary |scope global docker|scope global br-" |awk '{print $2}' | cut -d "/" -f1)

            # Catch Prefix
            PREFIX=$(ip a |egrep inet |egrep "scope global " |egrep -v  \
            "secondary |scope global docker|scope global br-" |awk '{print $2}' |awk '{print $NF}' FS=/)

            IFS=. read -r i1 i2 i3 i4 <<< $IP
            IFS=. read -r xx m1 m2 m3 m4 <<< $(for a in $(seq 1 32);
            do
            if [ $(((a - 1) % 8)) -eq 0 ]; then
                echo -n .;
            fi;
            if [ $a -le $PREFIX ]; then
                echo -n 1;
            else echo -n 0;
            fi;
            done)
            IP=$(printf "%d.%d.%d.%d\n" "$((i1 & (2#$m1)))" "$((i2 & (2#$m2)))" "$((i3 & (2#$m3)))" "$((i4 & (2#$m4)))")
            SOURCE_NETWORK="$IP"/"$PREFIX"
            fi

            # Swarm filter rules (This assumes docker nodes will be on the same network.
            # If there are multiple docker node networks, define all of them as source network.)
            $I -I INPUT 5 -p tcp -m state --state NEW -m tcp -s $SOURCE_NETWORK --dport 2377 -j ACCEPT
            $I -I INPUT 6 -p tcp -m state --state NEW -m tcp -s $SOURCE_NETWORK --dport 7946 -j ACCEPT
            $I -I INPUT 7 -p udp -m state --state NEW -m udp -s $SOURCE_NETWORK --dport 7946 -j ACCEPT
            $I -I INPUT 8 -p tcp -m state --state NEW -m tcp -s $SOURCE_NETWORK --dport 4789 -j ACCEPT
            $I -I INPUT 9 -p udp -m state --state NEW -m udp -s $SOURCE_NETWORK --dport 4789 -j ACCEPT
            $I -I INPUT 10 -p udp -m udp -s $SOURCE_NETWORK --sport 500 --dport 500 -j ACCEPT
            $I -I INPUT 11 -p esp -s $SOURCE_NETWORK -j ACCEPT
            $I -I INPUT 12 -p ah -s $SOURCE_NETWORK -j ACCEPT
        }

        # Create iptables rules
        function configure_firewll_rules() {
            while true
            do
                echo -en "Is this will be a swarm node? [y/n]: "
                read isitOK
            case $isitOK in
                y|Y|Yes|yes|YES)

                # Flush rules
                $I -F INPUT

                # Create base pass rules
                add_base_iptables_rules

                # Create base reject rules
                add_swarm_iptables_rules
                break
            ;;
                n|N|No|NO|no)

                # Flush rules
                $I -F INPUT

                # Create base pass rules
                add_base_iptables_rules
                break
            ;;
            *) echo "Please type 'y' or 'n'"
            esac
            done

        }

        configure_firewll_rules
        sleep 2
        service iptables save
        ;;

    *) echo "Usage : $0 --disk /dev/sdX"; exit 1

    esac

else
        echo "You specified a wrong disk, usable disks are:"
        fdisk -l |egrep "/dev/sd|dev/xvd" |egrep -v "sda:|sda1|sda2|xvda:|xvda1|xvda2"
        echo "Please choise a correct one!"
        exit 1
fi
